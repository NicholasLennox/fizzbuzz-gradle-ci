package no.accelerate.fizzbuzz;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class FizzBuzzConverterTest {

    @Test
    void convert_nonFBNumber_shouldReturnNumberAsString() {
        // Arrange
        FizzBuzzConverter fizzBuzzConverter = new FizzBuzzConverter();
        int number = 1;
        String expected = "1";
        // Act
        String actual = fizzBuzzConverter.convert(number);
        // Assert
        assertEquals(expected, actual);
    }

    @Test
    void convert_fizzNumber_shouldReturnFizz() {
        // Arrange
        FizzBuzzConverter fizzBuzzConverter = new FizzBuzzConverter();
        int number = 3;
        String expected = "Fizz";
        // Act
        String actual = fizzBuzzConverter.convert(number);
        // Assert
        assertEquals(expected, actual);
    }

    @Test
    void convert_buzzNumber_shouldReturnBuzz() {
        // Arrange
        FizzBuzzConverter fizzBuzzConverter = new FizzBuzzConverter();
        int number = 5;
        String expected = "Buzz";
        // Act
        String actual = fizzBuzzConverter.convert(number);
        // Assert
        assertEquals(expected, actual);
    }

    @Test
    void convert_fizzBuzzNumber_shouldReturnFizzBuzz() {
        // Arrange
        FizzBuzzConverter fizzBuzzConverter = new FizzBuzzConverter();
        int number = 15;
        String expected = "FizzBuzz";
        // Act
        String actual = fizzBuzzConverter.convert(number);
        // Assert
        assertEquals(expected, actual);
    }
}